<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ConnexionController extends Controller
{
    public function formulaire()
    {   //va afficher le formulaire de connexion
        return view('connexion');
    }

    public function traitement()
    {
        request()->validate([
            'email' => ['required', 'email'],
            'password' => ['required', 'min:8'],
        ]);

        $resultat = auth()->attempt([   //permet à l'utilisateur d'être vérifié et reconnu ds la BDD et de rester connecté toute la sessiion
            'email' => request('email'),
            'password' => request('password'),
        ]);

        //var_dump($resultat); //retourne true si l'utilisateur existe, false sinon

        if($resultat){

            flash("Vous êtes maintenant connecté")->success();

            return redirect('/mon-compte');
        }

        return back()->withInput()->withErrors([ //redirige vers la page précédente si false, withInput permet de garder l'adresse mail entrée par le user, withÈrrors affiche les erreurs
            'email' => 'Vos identifiants sont incorrects'

        ]);
    }
}
