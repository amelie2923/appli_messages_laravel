<?php

namespace App\Http\Controllers;

use App\Utilisateur;

class InscriptionController extends Controller
{
    public function formulaire()
    {
        return view('inscription');
    }

    public function traitement()
    {
        request()->validate([        //permet de  vérifier les champs de toute la requête avant de la valider
            'email' => ['required', 'email'], //requis, vérifie si c'est bien un mail,
            'password' => ['required', 'confirmed', 'min:8'], //confirmed vérifie directement password_confirmation, min:8 défini le minimum de caractères à 8
            'password_confirmation' => ['required'],
        ], [
            'password.min' => 'Pour des raisons de sécurité, votre mot de passe doit faire :min caractères' //message personnalisé pour un seul formulaire
        ]);
    
    
        $utilisateur = Utilisateur::create([  //fonction create = new + save
            'email' => request('email'),
            'mot_de_passe' => bcrypt(request('password')),
        ]);
    
        return "Nous avons reçu votre email qui est " . request('email'); //request est une fonction "magique", ici remplace le $_POST['email'] en php
    }
}
