<?php

namespace App\Http\Controllers;

use App\Reponse;

class ReponsesController extends Controller
{
    public function reponse(){

        request()->validate([
        'contenu' => ['required'] 
     ]);

     Reponse::create([
            // ou auth()->id(), récupère l'id de la personne connectée
            'utilisateur_id' => auth()->user()->id,
            //message_id, récupère l'id du message à laquel on répond
            'message_id' => $this->message()->id,
            // stocke le message dans la ligne contenu de la table messages
            'contenu' => request('contenu'), 
        ]);

        flash("Votre réponse a bien été publiée")->success();
        return back();
    }
}
    
