<?php

namespace App\Http\Controllers;
// pour importer le msg
use App\Message; 

class MessagesController extends Controller
{
    public function nouveau()
    {
        request()->validate([
           'message' => ['required'] 
        ]);

        Message::create([
            // ou auth()->id(), récupère l'id de la personne connectée
            'utilisateur_id' => auth()->user()->id, 
            // stocke le message dans la ligne contenu de la table messages
            'contenu' => request('message'), 
        ]);

        flash("Votre message a bien été publié")->success();
        return back();
    }
}
