<?php

namespace App\Http\Controllers;

use App\Utilisateur;
use App\Mail\NouveauSuiveur;
use Illuminate\Support\Facades\Mail;

class SuivisController extends Controller
{
    public function nouveau()
    {
        $utilisateurQuiVaSuivre = auth()->user(); //récupère l'utilisateur connecté
        $utilisateurQuiVaEtreSuivi = Utilisateur::where('email', request('email'))->firstOrFail(); //récupère l'email de l'utilisateur suivi
    
        $utilisateurQuiVaSuivre->suivis()->attach($utilisateurQuiVaEtreSuivi); //on attache l'utilisateur qui va etre suivi dans la liste des suivis de l'utilisateur qui va suivre
        
        Mail::to($utilisateurQuiVaEtreSuivi)->send(new NouveauSuiveur($utilisateurQuiVaSuivre)); //envoi un mail au suivi pour lui informer qu'il a un nouveau suiveur

        flash("Vous suivez maintenant {$utilisateurQuiVaEtreSuivi->email}.")->success();
        return back();
    }

    public function enlever()
    {
        $utilisateurQuiSuit = auth()->user();
        $utilisateurQuiEstSuivi = Utilisateur::where('email', request('email'))->firstOrFail();

        $utilisateurQuiSuit->suivis()->detach($utilisateurQuiEstSuivi);

        flash("Vous ne suivez plus {$utilisateurQuiEstSuivi->email}.")->success();
        return back();
    }

}
