<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CompteController extends Controller
{
    public function accueil()
    {

        //var_dump(auth()->check()); //vérifie si l'utilisateur est connecté

        //var_dump(auth()->guest()); //vérifie si l'utilistaeur n'est pas connecté

        return view('mon-compte');
    }

    public function deconnexion()
    {

        flash("Vous êtes maintenant déconnecté")->success();
        auth()->logout();
        return redirect('/');
    }

    public function modificationMotDePasse()
    {
        request()->validate([    //règles de validation mot de passe
            'password' => ['required', 'confirmed', 'min:8'],
            'password_confirmation' => ['required'],
        ]);

        /*$utilisateur = auth()->user();  //récupère l'utilisateur connecté et le stocke ds une variable

        $utilisateur->mot_de_passe = bcrypt(request('password')); 
        $utilisateur->save(); //mettre à jour l'utilisateur*/


        auth()->user()->update([
            'mot_de_passe' => bcrypt(request('password')),
        ]); //revient au même que les 3 lignes commentées

        flash("Votre mot de passe a bien été mis à jour.")->success();

        return redirect('/mon-compte');
    }

    public function modificationAvatar()
    {
        request()->validate([
            'avatar' => ['required', 'image'],
        ]);

        $path = request('avatar')->store('avatars', 'public'); //récupère l'avatar et le stock ds le dossier avatars dans le sous dossier public public/storage/avatars, $path renvoie le chemin de l'img

        auth()->user()->update([
            'avatar' => $path,                      //on stocke le chemin $path dans la colonne avatar
        ]);

        flash("Votre avatar a bien été mis à jour")->success();

        return back();
    }
}
