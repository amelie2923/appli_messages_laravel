<?php

namespace App\Http\Controllers;

use App\Utilisateur;
use App\Message;

class UtilisateursController extends Controller
{
    public function liste()
    {
        $utilisateurs = Utilisateur::all(); //retourne tous nos utilisateurs
        
        return view('utilisateurs', [
            'utilisateurs' => $utilisateurs,
        ]);
    }

    public function voir()
    {

        $email = request('email'); //voir si l'email de la personne s'affiche bien

        $utilisateur = Utilisateur::where('email', $email)->firstOrFail();  //récupère la 1ère et seule ligne sql email de l'utilisateur dans le model eloquent, retourne une 404 si fail (meme chose que dessous)
        
        /*if (is_null($utilisateur)){  //si pas d'utilisateur identifié on renvoie une 404
            abort(404);
        }*/

        //$messages = Message::where('utilisateur_id', $utilisateur->id)->latest()->get(); //récupère tous les messages liés à l'id de l'utilisateur


        return view('utilisateur', [      
            'utilisateur' => $utilisateur,  //utilisateur récupéré grâce à la requête sql passée en variable pour y avoir accès dans la vue
            ]);
    }
}
