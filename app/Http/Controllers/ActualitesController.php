<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ActualitesController extends Controller
{
    public function liste()
    {
        $messages = auth()->user() //on récupère le user connecté
        ->suivis //on récupère les utilisateurs qu'il suit
        ->load('messages') //eager loading dans la doc laravel, permet de faire une seule requete sql au lieu d'une requete par user
        ->flatMap->messages  //l'attribut map va transformer chaque personne qu'il suit en liste de message
        //flatten (flattMap raccourci) evite d'avoir un tableau dans un tableau, on a un seul tableau avec tous les msg des utilisateurs
        ->sortByDesc->created_at; //l'attribut sortByDesc trie les msg par heure, les derniers messages seront au début
        


        return view('actualites', [    //retourne la vue actualités
            'messages' => $messages,   //permet de passer la variable messages à la vue
        ]);
            
    }
}
