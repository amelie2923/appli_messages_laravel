<?php

namespace App; //car on se situe ds le dossier App
use Illuminate\Database\Eloquent\Model; //classe qui contient toutes les fonctions php dont on a besoin pr la bdd
use Illuminate\Contracts\Auth\Authenticatable; //classe qui autorise l'authentification (php)
use Illuminate\Auth\Authenticatable as BasicAuthenticatable; //classe Laravel qui permet d'automatiser ttes les méthodes requises par authenticatable php (à renommer pour ne pas confondre)

class Utilisateur extends Model implements Authenticatable{

    use BasicAuthenticatable;

    protected $fillable = ['email', 'mot_de_passe', 'avatar']; //permet d'éviter les massassignmentexception en précisant les champs autorisés dans le formulaire

    public function messages()
    {
        return $this->hasMany(Message::class)->latest();  //relation qui dit qu'un utilisateur peut avoir plusieurs messages
    }

    public function suivis()
    {
        return $this->belongsToMany(Utilisateur::class, 'suivis', 'suiveur_id', 'suivi_id');  //relation d'utilisateur à utilisateur, table suivis va contenir la relation entre les 2, suiveur = utilisateur; suivi = personne passive
    }


    public function suit($utilisateur)
    {
        return $this->suivis()->where('suivi_id', $utilisateur->id)->exists(); //récupère tous les suivis 
    }

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->mot_de_passe;  //fonction récupérée dans vendor/laravel/framework/src/illuminate/authauthenticatable qui retourne le champs password or notre champ s'appelle mot_de_passe donc on réécrit la fonction
    }

    /**
     * Get the column name for the "remember me" token.
     *
     * @return string
     */
    public function getRememberTokenName() //comme on utilise pas le remember token on réécrit la fonction et on retourne une chaine vide
    {
        return '';
    }
}