<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NouveauSuiveur extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $suiveur; //permet de rendre la variable publique et l'afficher ds la vue

    public function __construct($suiveur)
    {
        $this->suiveur = $suiveur;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->subject('Vous avez un nouveau suiveur!') //change le subject par defaut du mail
        ->markdown('mails.nouveau_suiveur'); //vue nouveau_suiveur dans le dossier mails
    }
}
