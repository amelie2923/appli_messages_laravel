<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reponse extends Model
{
    protected $fillable = ['utilisateur_id', 'message_id', 'contenu'];
}
