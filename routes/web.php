<?php

use App\Utilisateur;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::view('/', 'welcome');  retourne la vue welcome /resources/views

// Route::get('/a-propos', function () {
//     return view('a-propos');
// });

// Route::get('/bonjour/{prenom}', function () {
//     return view('bonjour', [
//         'prenom' => request('prenom'),
//     ]
// );
// });

Route::get('/inscription', 'InscriptionController@formulaire'); //get retourne la vue du form
Route::post('/inscription', 'InscriptionController@traitement'); //post envoie les données en traitement

Route::get('/connexion', 'ConnexionController@formulaire'); //retourne la vue du forlulaire
Route::post('/connexion', 'ConnexionController@traitement'); // envoie les données du form en traitement

Route::get('/', 'UtilisateursController@liste'); // classe@methode

Route::get('/mon-compte', 'CompteController@accueil')->middleware('App\Http\Middleware\Auth'); 
Route::get('/actualites', 'ActualitesController@liste')->middleware('App\Http\Middleware\Auth'); 
Route::get('/deconnexion', 'CompteController@deconnexion');
Route::get('/modification-mot-de-passe', 'CompteController@accueil')->middleware('App\Http\Middleware\Auth');
Route::post('/modification-mot-de-passe', 'CompteController@modificationMotDePasse')->middleware('App\Http\Middleware\Auth');
Route::post('/modification-avatar', 'CompteController@modificationAvatar')->middleware('App\Http\Middleware\Auth');

Route::post('/messages', 'MessagesController@nouveau')->middleware('App\Http\Middleware\Auth');
Route::post('/reponses', 'ReponsesController@reponse')->middleware('App\Http\Middleware\Auth');

Route::get('/{email}/suivis', 'SuivisController@nouveau')->middleware('App\Http\Middleware\Auth');
Route::post('/{email}/suivis', 'SuivisController@nouveau')->middleware('App\Http\Middleware\Auth');
Route::delete('/{email}/suivis', 'SuivisController@enlever')->middleware('App\Http\Middleware\Auth');

Route::get('/{email}', 'UtilisateursController@voir'); //route à laisser à la fin pour qu'elle ne soit pas prioritaire 


/*
On peut aussi créer des groupes de routes pr les routes ayant le même middleware : #endregion

Route::group([
    'middleware' => 'App\Http\Middleware\Auth',
], function () {
    Route::get('/mon-compte', 'CompteController@accueil');
    Route::get('/deconnexion', 'CompteController@deconnexion');
    Route::post('/modification-mot-de-passe', 'CompteController@modificationMotDePasse');

    Route::post('/messages', 'MessagesController@nouveau');
});

*/