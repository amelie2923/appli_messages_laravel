<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AjouterColonneAvatarAuxUtilisateurs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('utilisateurs', function (Blueprint $table) {     //table = mettre à jour une table existante
            $table->string('avatar')->nullable();                      //nullable = peut etre nul
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('utilisateurs', function (Blueprint $table) {     //table = mettre à jour une table existante
            $table->dropColumn('avatar');                     //nullable = peut etre nul
        });
    }
}
