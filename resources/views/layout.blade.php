<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title> 
        
        <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.2/css/bulma.css" />   
    </head>
    <body>
        {{-- jQuery pour activer le toggle--}}
        <script>
        $(document).ready(function() {

            // Check for click events on the navbar burger icon
            $(".navbar-burger").click(function() {
          
                // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
                $(".navbar-burger").toggleClass("is-active");
                $(".navbar-menu").toggleClass("is-active");
          
            });
          });
          </script>

        <nav class="navbar is-danger">
                <div class="navbar-brand">
                  <a role="button" class="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="myNavbar">
                    <span aria-hidden="true"></span>
                    <span aria-hidden="true"></span>
                    <span aria-hidden="true"></span>
                  </a>
                </div>
              
                <div id="myNavbar" class="navbar-menu">
                  <div class="navbar-start">
                        @include('partials.navbar-item', ['lien' => '/', 'texte' => 'Accueil'])
                        @auth
                            @include('partials.navbar-item', ['lien' => '/actualites', 'texte' => 'Actualités'])
                            @include('partials.navbar-item', ['lien' => auth()->user()->email, 'texte' => auth()->user()->email])
                        @endauth
                  </div>
              
                  <div class="navbar-end">
                        @auth
                            @include('partials.navbar-item', ['lien' => '/mon-compte', 'texte' => 'Mon compte'])
                            @include('partials.navbar-item', ['lien' => '/deconnexion', 'texte' => 'Déconnexion'])
                        @else
                            @include('partials.navbar-item', ['lien' => '/connexion', 'texte' => 'Connexion'])
                            @include('partials.navbar-item', ['lien' => '/inscription', 'texte' => 'Inscription'])
                        @endauth
                  </div>
                </div>
        </nav>
              
        <div class="container">
            @include('flash::message')

            @yield('contenu')
        </div>
    </body>
</html>