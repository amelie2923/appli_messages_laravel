@extends('layout')

@section('contenu')
    <div class="section">
        <h1 class="title is-1">     
            Actualités
        </h1>

        @foreach ($messages as $message)    <!-- récupère tous les messages des utilisateurs -->
            <hr>
            <p>
                <strong>{{ $message->created_at }}</strong><br>
                {{ $message->contenu }}

                {{ $message->id }}
            </p>
        @endforeach
    </div>

@endsection
