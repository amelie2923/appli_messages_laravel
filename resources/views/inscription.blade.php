@extends('layout')

@section('contenu')
    <form action="/inscription" method="post" class="section">
        {{ csrf_field() }}  <!--vérifie que le formulaire est bien envoyé de notre site-->
        
        <div class="field">
            <label class="label">Adresse email</label>
            <div class="control">
                <input class="input" type="email" name="email" value="{{ old('email') }}"> <!-- value old = garde la valeur dans le champ même si il y a une erreur ds le form-->
            </div>
            @if($errors->has('email'))
            <p class="help is-danger">{{ $errors->first('email') }}</p> <!--affiche la 1ere erreur rencontrée-->
            @endif
        </div>
        
        <div class="field">
            <label class="label">Mot de passe</label>
            <div class="control">
                <input class="input" type="password" name="password"> <!-- value old = garde la valeur dans le champ même si il y a une erreur ds le form-->
            </div>
            @if($errors->has('password'))
            <p class="help is-danger">{{ $errors->first('password') }}</p> <!--affiche la 1ere erreur rencontrée-->
            @endif
        </div>
        
        <div class="field">
            <label class="label">Confirmer mot de passe</label>
            <div class="control">
                <input class="input" type="password" name="password_confirmation"> <!-- value old = garde la valeur dans le champ même si il y a une erreur ds le form-->
            </div>
            @if($errors->has('password_confirmation'))
            <p class="help is-danger">{{ $errors->first('password_confirmation') }}</p> <!--affiche la 1ere erreur rencontrée-->
            @endif
        </div>

        <div class="field">
            <div class="control">
                <button class="button is-link" type="submit">M'inscrire</button>
            </div>
        </div>
    </form>
@endsection