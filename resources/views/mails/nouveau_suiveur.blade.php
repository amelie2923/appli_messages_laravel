@component('mail::message')

# Hey ! <!-- # représente un h1 en markdown, ## un h2-->

Vous avez un nouveau suiveur {{ $suiveur->email }}.

@endcomponent